﻿#include <tchar.h>
#include <windows.h>
#include <dxgi.h>
#include <dxgi1_2.h>
#include <dxgi1_3.h>

#include <d3d12.h>
#include <ngfx/ngfx.h>
#include <ngfx/os/Window.h>

#ifndef SAFE_RELEASE
#define SAFE_RELEASE(p)	{ if (p) { (p)->Release(); (p)=nullptr; } }
#endif

using namespace ngfx;

namespace
{
	static const size_t BUFFER_NUM = 2;

	ID3D12Debug* g_pDebug;
	Window* g_pWindow;
	IDXGISwapChain* g_pSwapChain;
	ID3D12Device* g_pDevice;
	ID3D12CommandQueue* g_pCommandQueue;
	ID3D12CommandAllocator* g_pCommandAllocator;
	ID3D12GraphicsCommandList* g_pCommandList;
	ID3D12Resource* g_apBackBuffer[BUFFER_NUM];
}

/**
* @brief ウィンドウのリサイズ
*/
void ResizeWindow(const UINT& uiWidth, const UINT& uiHeight, void* pUserData)
{
	if (g_pSwapChain != nullptr)
	{
		DXGI_SWAP_CHAIN_DESC swapchainDesc;
		HRESULT hr = g_pSwapChain->GetDesc(&swapchainDesc);
		g_pSwapChain->ResizeBuffers(swapchainDesc.BufferCount, uiWidth, uiHeight, swapchainDesc.BufferDesc.Format, swapchainDesc.Flags);
	}
}

/**
* @brief 初期化
*/
int Initialize(HINSTANCE hInstance)
{
	// SampleWindow
	{
		HRESULT hr;
		// Device
		{
#if defined(DEBUG) || defined(_DEBUG)
			D3D12GetDebugInterface(IID_PPV_ARGS(&g_pDebug));
			if (g_pDebug)
			{
				g_pDebug->EnableDebugLayer();
			}
#endif
			hr = D3D12CreateDevice(
				NULL,						// pAdapter 
				D3D_FEATURE_LEVEL_11_0,		// MinimumFeatureLevel
				IID_PPV_ARGS(&g_pDevice)
			);
			if (FAILED(hr)) return -1;
		}

		// Command Queue
		{
			D3D12_COMMAND_QUEUE_DESC cmdQueueDesc;
			ZeroMemory(&cmdQueueDesc, sizeof(cmdQueueDesc));
			cmdQueueDesc.Type = D3D12_COMMAND_LIST_TYPE_DIRECT;
			cmdQueueDesc.Flags = D3D12_COMMAND_QUEUE_FLAG_NONE;
			cmdQueueDesc.Priority = 0;
			cmdQueueDesc.NodeMask = 0;
			hr = g_pDevice->CreateCommandQueue(&cmdQueueDesc, IID_PPV_ARGS(&g_pCommandQueue));
			if (FAILED(hr)) return -1;
		}

		// Command Allocator
		hr = g_pDevice->CreateCommandAllocator(D3D12_COMMAND_LIST_TYPE_DIRECT, IID_PPV_ARGS(&g_pCommandAllocator));
		if (FAILED(hr)) return -1;

		// Command List
		hr = g_pDevice->CreateCommandList(0, D3D12_COMMAND_LIST_TYPE_DIRECT, g_pCommandAllocator, nullptr, IID_PPV_ARGS(&g_pCommandList));
		if (FAILED(hr)) return -1;

		// Window
		g_pWindow = new ngfx::Window();
		WINDOW_CREATE_DESC desc;
		desc.Name = _T("Sample");
		desc.Width = 1280;
		desc.Height = 720;
		desc.hInstance = hInstance;
		desc.pUserData = nullptr;
		desc.ResizeCallback = ResizeWindow;
		hr = g_pWindow->Create(desc);
		if (FAILED(hr)) return -1;

		// DXGIFactory
		IDXGIFactory2* pFactory;
		{
			UINT uiFlags = 0;
#if defined(DEBUG) || defined(_DEBUG)
			uiFlags = DXGI_CREATE_FACTORY_DEBUG;
#endif
			hr = CreateDXGIFactory2(uiFlags, IID_PPV_ARGS(&pFactory));
			if (FAILED(hr)) return -1;
		}

		// IDXGISwapChain
		UINT uiCurrentWindowWidth, uiCurrentWindowHeight;
		g_pWindow->GetWindowSize(&uiCurrentWindowWidth, &uiCurrentWindowHeight);
		DXGI_SWAP_CHAIN_DESC swapChainDesc;
		ZeroMemory(&swapChainDesc, sizeof(DXGI_SWAP_CHAIN_DESC));
		swapChainDesc.BufferCount = 2;
		swapChainDesc.BufferDesc.Width = uiCurrentWindowWidth;
		swapChainDesc.BufferDesc.Height = uiCurrentWindowHeight;
		swapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
		swapChainDesc.BufferDesc.RefreshRate.Numerator = 60;
		swapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
		swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
		swapChainDesc.OutputWindow = g_pWindow->GetHWND();
		swapChainDesc.SampleDesc.Count = 1;
		swapChainDesc.SampleDesc.Quality = 0;
		swapChainDesc.Windowed = true;
		swapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_FLIP_DISCARD;
		hr = pFactory->CreateSwapChain(g_pCommandQueue, &swapChainDesc, &g_pSwapChain);
		if (FAILED(hr)) return -1;
		SAFE_RELEASE(pFactory);
	}

	return 0;
}

/**
* @brief 破棄
*/
void Finalize()
{
	SAFE_RELEASE(g_pSwapChain);
	SAFE_RELEASE(g_pCommandList);
	SAFE_RELEASE(g_pCommandAllocator);
	SAFE_RELEASE(g_pCommandQueue);
	SAFE_RELEASE(g_pDevice);
	SAFE_RELEASE(g_pDebug);
	delete g_pWindow;
}

/**
* @brief シーンのレンダリング
*/
void RenderScene()
{

}

/**
* @brief メイン関数
*/
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	int ret;
	// 初期化
	ret = Initialize(hInstance);
	if (ret != 0) return ret;

	// loop
	g_pWindow->Show();
	while (!g_pWindow->IsQuit())
	{
		g_pWindow->BeginFrame();
		RenderScene();
		g_pSwapChain->Present(0, 0);
	}

	// 破棄
	Finalize();
	return 0;
}