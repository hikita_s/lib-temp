echo off
pushd .
setlocal

set ARG_MODE=%1

set CMAKE="C:\Program Files\CMake\bin\cmake"
set MSBUILD="C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\MSBuild\15.0\Bin\MSBuild.exe"
set CMAKE_GENERATOR="Visual Studio 15 2017 Win64"
set BUILD_DIR=build

mkdir %BUILD_DIR%
cd %BUILD_DIR%

REM buildディレクトリの作成
CMAKE ../ -G %CMAKE_GENERATOR%

REM シェーダー生成物を含めて再びCMAKE
CMAKE ../ -G %CMAKE_GENERATOR%

endlocal
popd