if(__UTILITY_INCLUDED)
	return()
endif()
set(__UTILITY_INCLUDED TRUE)

#-------------------------------------------------------------------------------
# プリコンパイル済みヘッダの作成
# @param pc_h_file     プリコンパイル済みヘッダのhファイル
# @param pc_cpp_file   プリコンパイル済みヘッダのcppファイル
# @param pc_bin_file   pchファイル
#-------------------------------------------------------------------------------
function(precompiled_binary_create pc_h_file pc_cpp_file pc_bin_file)
  # TODO : /Yc"pch.h"みたいにしないと動かない？
  get_filename_component(precompiled_base_name ${pc_h_file} NAME)
  message("${pc_bin_file}")
  set_source_files_properties(${pc_cpp_file}
    PROPERTIES COMPILE_FLAGS "/Yc\"${precompiled_base_name}\" /Fp\"${pc_bin_file}\""
    OBJECT_OUTPUTS "${pc_bin_file}"
  )
endfunction()

#-------------------------------------------------------------------------------
# プリコンパイル済みヘッダの利用
# @param pc_h_file     プリコンパイル済みヘッダのhファイル
# @param pc_bin_file   pchファイル
# @param src_cpp_files プリコンパイル済みヘッダを利用するcppファイルのリスト
#-------------------------------------------------------------------------------
function(precompiled_header_use pc_h_file pc_bin_file src_cpp_files)
  set_source_files_properties(${src_cpp_files}
    PROPERTIES COMPILE_FLAGS "/Yu\"${pc_h_file}\" /FI\"${pc_h_file}\" /Fp\"${pc_bin_file}\""
    OBJECT_DEPENDS "${pc_bin_file}"
  )
endfunction()

#-------------------------------------------------------------------------------
# プリコンパイル済みヘッダの一括設定
# @param pc_h_file      プリコンパイル済みヘッダのhファイル
# @param pc_cpp_file    プリコンパイル済みヘッダのcppファイル
# @param pc_bin_file    pchファイル
# @param src_cpp_files  プリコンパイル済みヘッダを利用するcppファイルのリスト
#-------------------------------------------------------------------------------
function(precompiled_header pc_h_file pc_cpp_file pc_bin_file src_cpp_files)
  set(src_files ${src_cpp_files})
  list (REMOVE_ITEM src_files ${pc_cpp_file})  # pc_cpp_file自体が入っていた場合はsrc_cpp_filesから除去
  precompiled_binary_create(${pc_h_file} ${pc_cpp_file} ${pc_bin_file})
  precompiled_header_use(${pc_h_file} ${pc_bin_file} "${src_files}")
endfunction()