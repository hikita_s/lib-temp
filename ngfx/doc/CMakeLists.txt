cmake_minimum_required(VERSION 3.11)

#------------------------------------------------------------------------------------
# doxygen生成プロジェクトユーティリティ関数
#------------------------------------------------------------------------------------
function(document lang)
    project( doc_${lang} )
    #------------------------------------------------------------------------------------
    # doxygenカレントディレクトリから,ソースへの相対パスの作成
    #------------------------------------------------------------------------------------
    file(RELATIVE_PATH relative_NGFX_INCLUDE_DIR ${CMAKE_CURRENT_BINARY_DIR} ${NGFX_INCLUDE_DIR})
    file(RELATIVE_PATH relative_DOC_SRC_DIR ${CMAKE_CURRENT_BINARY_DIR} ${CMAKE_CURRENT_SOURCE_DIR}/src)

    #------------------------------------------------------------------------------------
    # doxygenへの入力ファイル,出力ファイルの設定
    #------------------------------------------------------------------------------------
    set(DOXYGEN_PROJECT_NAME "NGFX")
    set(DOXYGEN_INPUT "${relative_NGFX_INCLUDE_DIR} ${relative_DOC_SRC_DIR}")
    set(DOXYGEN_OUTPUT_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}")

    #------------------------------------------------------------------------------------
    # doxygenレイアウト変数設定
    #------------------------------------------------------------------------------------
    include(i18n.cmake)

    #------------------------------------------------------------------------------------
    # doxygen生成用のENABLED_SECTIONS定義
    # LANG_JP,LANG_EN,...
    #------------------------------------------------------------------------------------
    string(TOUPPER "LANG_${lang}" lang_tag)         # 大文字化
    set(DOXYGEN_ENABLED_SECTIONS "${lang_tag}")

    #------------------------------------------------------------------------------------
    # doxygenのコンフィグファイルをテンプレートから作成
    #------------------------------------------------------------------------------------
    set(DOXYGEN_CONFIG_FILE "${CMAKE_CURRENT_BINARY_DIR}/doc.config")
    configure_file (
        ${CMAKE_CURRENT_SOURCE_DIR}/../template/doc.config
        ${DOXYGEN_CONFIG_FILE}
    )
    configure_file (
        ${CMAKE_CURRENT_SOURCE_DIR}/../template/layout.xml
        ${CMAKE_CURRENT_BINARY_DIR}/layout.xml
    )

    add_custom_target(
        ${PROJECT_NAME}
        SOURCES ${include_files}
        COMMAND cmd /c $<SHELL_PATH:${DOXYGEN_PATH}> $<SHELL_PATH:${DOXYGEN_CONFIG_FILE}>
    )

    set_target_properties(${PROJECT_NAME} PROPERTIES FOLDER "doc" )
endfunction()

#------------------------------------------------------------------------------------
# doc_jp
#------------------------------------------------------------------------------------
add_subdirectory(jp)