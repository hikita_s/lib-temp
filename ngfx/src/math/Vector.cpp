﻿#include <ngfx/math/Vector.h>
#include <ngfx/core/Core.h>

namespace ngfx
{
	//// 明示的テンプレートのインスタンス化
	template class Vector4TImpl<float>;
	template class Vector3TImpl<float>;
	template class Vector2TImpl<float>;
	template class Vector4T<float>;
	template class Vector3T<float>;
	template class Vector2T<float>;

	template class Vector4TImpl<int>;
	template class Vector3TImpl<int>;
	template class Vector2TImpl<int>;
	template class Vector4T<int>;
	template class Vector3T<int>;
	template class Vector2T<int>;
	//template class Color4T<float>;

	//// POD check
	NGFX_STATIC_ASSERT(std::is_pod<Vector4fImpl>(), "Vector4fImpl is POD ");
	NGFX_STATIC_ASSERT(std::is_pod<Vector3fImpl>(), "Vector3fImpl is POD ");
	NGFX_STATIC_ASSERT(std::is_pod<Vector2fImpl>(), "Vector2fImpl is POD ");

	NGFX_STATIC_ASSERT(std::is_pod<Vector4iImpl>(), "Vector4iImpl is POD ");
	NGFX_STATIC_ASSERT(std::is_pod<Vector3iImpl>(), "Vector3iImpl is POD ");
	NGFX_STATIC_ASSERT(std::is_pod<Vector2iImpl>(), "Vector2iImpl is POD ");
}