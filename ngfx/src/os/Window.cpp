﻿#include <ngfx/os/Window.h>
namespace ngfx
{
	Window::Window()
		: m_hWnd(nullptr)
		, m_MSG()
		, m_ResizeCallback(nullptr)
		, m_pUserData(nullptr)
	{
	}

	Window::~Window()
	{
	}

	HRESULT Window::Create(const WINDOW_CREATE_DESC & desc)
	{
		if (desc.ResizeCallback == nullptr) return -1;
		m_ResizeCallback = desc.ResizeCallback;
		m_pUserData = desc.pUserData;

		// ウィンドウクラスを作成
		WNDCLASSEX wcl;
		wcl.cbSize = sizeof(WNDCLASSEX);
		wcl.hInstance = desc.hInstance;
		wcl.lpszClassName = desc.Name;
		wcl.lpfnWndProc = WindowProc;
		wcl.style = NULL;
		wcl.hIcon = NULL;
		wcl.hIconSm = NULL;
		wcl.hCursor = LoadCursor(NULL, IDC_ARROW);
		wcl.lpszMenuName = NULL;
		wcl.cbClsExtra = 0;
		wcl.cbWndExtra = 0;
		wcl.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
		// システムにウィンドウクラスを登録
		if (!RegisterClassEx(&wcl)) return E_FAIL;

		// ウィンドウクラスからウィンドウを作成
		m_hWnd = CreateWindowEx(
			NULL,					// 拡張ウィンドウスタイル
			desc.Name,				// 登録されているクラス名
			desc.Name,				// ウィンドウ名
			WS_OVERLAPPEDWINDOW,	// ウィンドウスタイル
			CW_USEDEFAULT,			// ウィンドウの横方向の位置
			CW_USEDEFAULT,			// ウィンドウの縦方向の位置
			CW_USEDEFAULT,			// ウィンドウの幅
			CW_USEDEFAULT,			// ウィンドウの高さ
			NULL,					// 親ウィンドウまたはオーナーウィンドウのハンドル
			NULL,					// メニューハンドルまたは子識別子
			desc.hInstance,			// アプリケーションインスタンスのハンドル
			NULL					// ウィンドウ作成データ
		);
		if (m_hWnd == nullptr) return E_FAIL;
		SetWindowLongPtr(m_hWnd, GWLP_USERDATA, reinterpret_cast<LONG_PTR>(this));

		// Windowサイズの設定
		RECT rectWindow;
		RECT rectClient;
		GetWindowRect(m_hWnd, &rectWindow);
		GetClientRect(m_hWnd, &rectClient);
		const UINT newW = desc.Width + ((rectWindow.right - rectWindow.left) - (rectClient.right - rectClient.left));
		const UINT newH = desc.Height + ((rectWindow.bottom - rectWindow.top) - (rectClient.bottom - rectClient.top));
		SetWindowPos(m_hWnd, NULL, 0, 0, newW, newH, (SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_NOMOVE));

		return S_OK;
	}

	void Window::Show() const
	{
		ShowWindow(m_hWnd, SW_SHOWNORMAL);
	}

	bool Window::IsQuit() const noexcept
	{
		return m_MSG.message == WM_QUIT;
	}

	void Window::GetWindowSize(UINT * pWidth, UINT * pHeight)
	{
		RECT rc;
		if (GetClientRect(m_hWnd, &rc) == false) return;
		*pWidth = rc.right - rc.left;
		*pHeight = rc.bottom - rc.top;
		return;
	}

	void Window::BeginFrame()
	{
		// Poll Message
		if (PeekMessage(&m_MSG, NULL, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&m_MSG);
			DispatchMessage(&m_MSG);
		}
	}


	LRESULT Window::WindowProcImpl(HWND hwnd, UINT Msg, WPARAM wParam, LPARAM lParam)
	{
		switch (Msg)
		{
		case WM_DESTROY:
		{
			PostQuitMessage(0);
		}
		break;
		case WM_SIZE:
		{
			if (wParam != SIZE_MINIMIZED)
			{
				const UINT w = static_cast<UINT>(LOWORD(lParam));
				const UINT h = static_cast<UINT>(HIWORD(lParam));
				m_ResizeCallback(w, h, m_pUserData);
			}
		}
		break;
		default:
			return DefWindowProc(hwnd, Msg, wParam, lParam);
		}
		return 0;
	}

	LRESULT Window::WindowProc(HWND hwnd, UINT Msg, WPARAM wParam, LPARAM lParam)
	{
		Window* pWindow = reinterpret_cast<Window*>(GetWindowLongPtr(hwnd, GWLP_USERDATA));
		if (pWindow != nullptr)
		{
			return pWindow->WindowProcImpl(hwnd, Msg, wParam, lParam);
		}
		else
		{
			return DefWindowProc(hwnd, Msg, wParam, lParam);
		}
	}

}