﻿#include <ngfx/core/Result.h>

namespace ngfx
{
	const Result Result::OK(0x0);
	const Result Result::FAIL(0x80000000);

	Result::Result(const uint32_t & code) noexcept
		: m_Code(code)
	{}

	Result::Result()
		: m_Code(0x80000000)
	{}

	Result::~Result()
	{}

	bool Result::IsSucceeded() const noexcept
	{
		return m_Code == 0x0;
	}

	bool Result::IsFailed() const noexcept
	{
		return m_Code != 0x0;
	}

}
