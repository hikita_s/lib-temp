﻿#include <ngfx/core/Types.h>
#include <ngfx/core/Util.h>
#include <ngfx/core/IMemoryAllocator.h>
#include <ngfx/core/HeapMemoryAllocator.h>

namespace ngfx
{

	LOG_LEVEL g_LogLevel = LOG_LEVEL_DISABLED;
	HeapMemoryAllocator g_HeapMemoryAllocator;
	IMemoryAllocator* g_pDefaultMemoryAllocator = &g_HeapMemoryAllocator;

	void NGFX_CALL BreakPoint()
	{
		DebugBreak();
	}

	void NGFX_CALL OutputString(const char * string, const char * file, const int & line) noexcept
	{
		char buf[NGFX_OUTPUT_STRING_BUFFER_MAX_SIZE];
		sprintf_s(buf, "%s(%d):%s\n", file, line, string);
		OutputDebugString(buf);
	}

	void NGFX_CALL SetDefaultMemoryAllocator(IMemoryAllocator * pMemoryAllocator) noexcept
	{
		g_pDefaultMemoryAllocator = pMemoryAllocator;
	}

	IMemoryAllocator *NGFX_CALL GetDefaultMemoryAllocator() noexcept
	{
		return g_pDefaultMemoryAllocator;
	}

	void NGFX_CALL SetLogLevel(const LOG_LEVEL & level) noexcept
	{
		g_LogLevel = level;
	}

	void NGFX_CALL OutputLog(const LOG_LEVEL & level, const char * string, const char * file, const int & line)
	{
		if (g_LogLevel == LOG_LEVEL_DISABLED) return;
		if (level == g_LogLevel)
		{
			char logLevelString[32];
			#define LOG_LEVEL_CASE(x)						\
					case LOG_LEVEL_##x:						\
					{															\
						char* str = (#x);								\
						size_t len = strlen(#x);					\
						memcpy(logLevelString, str, len);	\
						logLevelString[len] = '\0';				\
					}															\
					break; 
			switch (level)
			{
				LOG_LEVEL_CASE(FATAL)
				LOG_LEVEL_CASE(ERROR)
				LOG_LEVEL_CASE(WARN)
				LOG_LEVEL_CASE(INFO)
				LOG_LEVEL_CASE(DEBUG)
				LOG_LEVEL_CASE(TRACE)
			default:
				break;
			}
			#undef LOG_LEVEL_CASE
			char buf[NGFX_OUTPUT_STRING_BUFFER_MAX_SIZE];
			sprintf_s(buf, "[%s] %s", logLevelString, string);
			OutputString(buf, file, line);
		}
	}
}
