﻿#include <ngfx/core/HeapMemoryAllocator.h>
#include <ngfx/ngfx.h>

namespace ngfx
{
	HeapMemoryAllocator::HeapMemoryAllocator()
		: m_pFront(nullptr)
		, m_pBack(nullptr)
		, m_CurrentMemoryBlockId(0)
	{
	}

	HeapMemoryAllocator::~HeapMemoryAllocator()
	{
	}

	Result HeapMemoryAllocator::Allocate(void ** pRet, const size_t & size, const char * file, const int & line)
	{
		void* p = malloc(size + sizeof(MemoryHeader));
		if (p == nullptr) return Result::FAIL;
		MemoryHeader* pMemoryHeader = reinterpret_cast<MemoryHeader*>(p);
		pMemoryHeader->Size = size;
		pMemoryHeader->File = file;
		pMemoryHeader->Line = line;
		pMemoryHeader->Id = m_CurrentMemoryBlockId;
		pMemoryHeader->pNext = nullptr;
		pMemoryHeader->pPrev = m_pBack;
		if (m_pBack != nullptr) m_pBack->pNext = pMemoryHeader;
		if (m_pFront == nullptr) m_pFront = pMemoryHeader;
		m_pBack = pMemoryHeader;
		*pRet = reinterpret_cast<void*>( reinterpret_cast<intptr_t>(p) + sizeof(MemoryHeader) );
		m_CurrentMemoryBlockId++;
		return Result::OK;
	}

	Result HeapMemoryAllocator::Free(void* p)
	{
		if (p == nullptr) return Result::FAIL;
		MemoryHeader* pMemoryHeader = reinterpret_cast<MemoryHeader*>(reinterpret_cast<intptr_t>(p) - sizeof(MemoryHeader));
		MemoryHeader* pNext = pMemoryHeader->pNext;
		MemoryHeader* pPrev = pMemoryHeader->pPrev;
		if (pNext != nullptr) pNext->pPrev = pPrev;
		if (pPrev != nullptr) pPrev->pNext = pNext;
		if (m_pBack == pMemoryHeader) m_pBack = pPrev;
		if (m_pFront == pMemoryHeader) m_pFront = pNext;
		free(pMemoryHeader);
		return Result::OK;
	}


	HeapMemoryAllocator::AllocatedMemoryIterator HeapMemoryAllocator::AllocateMemoryIteratorBegin() const noexcept
	{
		return AllocatedMemoryIterator(m_pFront);
	}

	HeapMemoryAllocator::AllocatedMemoryIterator HeapMemoryAllocator::AllocateMemoryIteratorEnd() const noexcept
	{
		return AllocatedMemoryIterator(nullptr);
	}

	HeapMemoryAllocator::AllocatedMemoryIterator::AllocatedMemoryIterator(MemoryHeader * pHeader)
		: m_pHeader(pHeader)
	{
	}

	HeapMemoryAllocator::AllocatedMemoryIterator::~AllocatedMemoryIterator()
	{
	}

	HeapMemoryAllocator::AllocatedMemoryIterator& HeapMemoryAllocator::AllocatedMemoryIterator::operator++()
	{
		m_pHeader = m_pHeader->pNext;
		return *this;
	}

	HeapMemoryAllocator::AllocatedMemoryIterator HeapMemoryAllocator::AllocatedMemoryIterator::operator++(int)
	{
		m_pHeader = m_pHeader->pNext;
		return *this;
	}

	bool HeapMemoryAllocator::AllocatedMemoryIterator::operator==(const AllocatedMemoryIterator & iterator)
	{
		return m_pHeader == iterator.m_pHeader;
	}

	bool HeapMemoryAllocator::AllocatedMemoryIterator::operator!=(const AllocatedMemoryIterator & iterator)
	{
		return m_pHeader != iterator.m_pHeader;
	}

	const MemoryBlockInfo& HeapMemoryAllocator::AllocatedMemoryIterator::operator->()
	{
		return *m_pHeader;
	}

	const MemoryBlockInfo & HeapMemoryAllocator::AllocatedMemoryIterator::operator*()
	{
		return *m_pHeader;
	}
};
