﻿#include <ngfx/core/Memory.h>

namespace ngfx
{

	void* NGFX_CALL AllocateImpl(IMemoryAllocator * pMemoryAllocator, const size_t & size, const char * file, const int & line)
	{
		NGFX_ASSERT(pMemoryAllocator != nullptr);
		void* p = nullptr;
		const Result result = pMemoryAllocator->Allocate(&p, size, file, line);
		NGFX_ASSERT(result.IsSucceeded());
		return p;
	}

	void NGFX_CALL FreeImpl(IMemoryAllocator * pMemoryAllocator, void * ptr)
	{
		NGFX_ASSERT(pMemoryAllocator != nullptr);
		const Result result = pMemoryAllocator->Free(ptr);
		NGFX_ASSERT(result.IsSucceeded());
	}
}
