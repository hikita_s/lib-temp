﻿#ifndef NGFX_INCLUDE_VECTORIMPL_GENERAL_INL
#define NGFX_INCLUDE_VECTORIMPL_GENERAL_INL
#include <ngfx/math/Vector.h>

#ifdef NGFX_VEC_TARGET_GENERAL
namespace ngfx
{
	//-------------------------------------------------------------------------------------------------------------
	// Vector4TImpl
	template<class Type>
	inline Type& Vector4TImpl<Type>::operator[](const size_t & index)
	{
		return arr[index];
	}

	template<class Type>
	inline Vector4TImpl<Type>::operator Type*()
	{
		return arr;
	}

	template<class Type>
	inline Vector4TImpl<Type>::operator const Type*() const
	{
		return arr;
	}

	template<class Type>
	inline Type Vector4TImpl<Type>::GetX() const noexcept
	{
		return x;
	}

	template<class Type>
	inline Type Vector4TImpl<Type>::GetY() const noexcept
	{
		return y;
	}

	template<class Type>
	inline Type Vector4TImpl<Type>::GetZ() const noexcept
	{
		return z;
	}

	template<class Type>
	inline Type Vector4TImpl<Type>::GetW() const noexcept
	{
		return w;
	}

	template<class Type>
	inline void Vector4TImpl<Type>::SetX(const Type & x) noexcept
	{
		this->x = x;
	}

	template<class Type>
	inline void Vector4TImpl<Type>::SetY(const Type & y) noexcept
	{
		this->y = y;
	}

	template<class Type>
	inline void Vector4TImpl<Type>::SetZ(const Type & z) noexcept
	{
		this->z = z;
	}

	template<class Type>
	inline void Vector4TImpl<Type>::SetW(const Type & w) noexcept
	{
		this->w = w;
	}

	template<class Type>
	inline void Vector4TImpl<Type>::Set(const Type & x, const Type & y, const Type & z, const Type & w) noexcept
	{
		this->x = x;
		this->y = y;
		this->z = z;
		this->w = w;
	}

	template<class Type>
	inline void Vector4TImpl<Type>::Add(Vector4TImpl* pDst, const Vector4TImpl & lhs, const Vector4TImpl & rhs)
	{
		pDst->x = lhs.x + rhs.x;
		pDst->y = lhs.y + rhs.y;
		pDst->z = lhs.z + rhs.z;
		pDst->w = lhs.w + rhs.w;
	}

	template<class Type>
	inline void Vector4TImpl<Type>::Sub(Vector4TImpl* pDst, const Vector4TImpl & lhs, const Vector4TImpl & rhs)
	{
		pDst->x = lhs.x - rhs.x;
		pDst->y = lhs.y - rhs.y;
		pDst->z = lhs.z - rhs.z;
		pDst->w = lhs.w - rhs.w;
	}

	template<class Type>
	inline void Vector4TImpl<Type>::Mul(Vector4TImpl* pDst, const Vector4TImpl & lhs, const Vector4TImpl & rhs)
	{
		pDst->x = lhs.x * rhs.x;
		pDst->y = lhs.y * rhs.y;
		pDst->z = lhs.z * rhs.z;
		pDst->w = lhs.w * rhs.w;
	}

	template<class Type>
	inline void Vector4TImpl<Type>::Div(Vector4TImpl* pDst, const Vector4TImpl & lhs, const Vector4TImpl & rhs)
	{
		pDst->x = lhs.x / rhs.x;
		pDst->y = lhs.y / rhs.y;
		pDst->z = lhs.z / rhs.z;
		pDst->w = lhs.w / rhs.w;
	}

	template<class Type>
	inline void Vector4TImpl<Type>::Length(Type* pDst, const Vector4TImpl & vec)
	{
		*pDst = static_cast<Type>(sqrtf(static_cast<float>(vec.x*vec.x + vec.y*vec.y + vec.z*vec.z + vec.w*vec.w)));
	}

	template<class Type>
	inline void Vector4TImpl<Type>::LengthSquared(Type* pDst, const Vector4TImpl & vec)
	{
		*pDst = vec.x*vec.x + vec.y*vec.y + vec.z*vec.z + vec.w*vec.w;
	}

	template<class Type>
	inline void Vector4TImpl<Type>::Normalize(Vector4TImpl* pDst, const Vector4TImpl& vec)
	{
		Type len;
		Length(&len, vec);
		if (len > 0)
		{
			pDst->x = pDst->x / len;
			pDst->y = pDst->y / len;
			pDst->z = pDst->z / len;
			pDst->w = pDst->w / len;
		}
		else
		{
			pDst->Set(0, 0, 0, 0);
		}
	}

	//-------------------------------------------------------------------------------------------------------------
	// Vector3TImpl
	template<class Type>
	inline Type& Vector3TImpl<Type>::operator[](const size_t & index)
	{
		return arr[index];
	}

	template<class Type>
	inline Vector3TImpl<Type>::operator Type*()
	{
		return arr;
	}

	template<class Type>
	inline Vector3TImpl<Type>::operator const Type*() const
	{
		return arr;
	}

	template<class Type>
	inline Type Vector3TImpl<Type>::GetX() const noexcept
	{
		return x;
	}

	template<class Type>
	inline Type Vector3TImpl<Type>::GetY() const noexcept
	{
		return y;
	}

	template<class Type>
	inline Type Vector3TImpl<Type>::GetZ() const noexcept
	{
		return z;
	}

	template<class Type>
	inline void Vector3TImpl<Type>::SetX(const Type & x) noexcept
	{
		this->x = x;
	}

	template<class Type>
	inline void Vector3TImpl<Type>::SetY(const Type & y) noexcept
	{
		this->y = y;
	}

	template<class Type>
	inline void Vector3TImpl<Type>::SetZ(const Type & z) noexcept
	{
		this->z = z;
	}

	template<class Type>
	inline void Vector3TImpl<Type>::Set(const Type & x, const Type & y, const Type & z) noexcept
	{
		this->x = x;
		this->y = y;
		this->z = z;
	}

	template<class Type>
	inline void Vector3TImpl<Type>::Add(Vector3TImpl* pDst, const Vector3TImpl & lhs, const Vector3TImpl & rhs)
	{
		pDst->x = lhs.x + rhs.x;
		pDst->y = lhs.y + rhs.y;
		pDst->z = lhs.z + rhs.z;
	}

	template<class Type>
	inline void Vector3TImpl<Type>::Sub(Vector3TImpl* pDst, const Vector3TImpl & lhs, const Vector3TImpl & rhs)
	{
		pDst->x = lhs.x - rhs.x;
		pDst->y = lhs.y - rhs.y;
		pDst->z = lhs.z - rhs.z;
	}

	template<class Type>
	inline void Vector3TImpl<Type>::Mul(Vector3TImpl* pDst, const Vector3TImpl & lhs, const Vector3TImpl & rhs)
	{
		pDst->x = lhs.x * rhs.x;
		pDst->y = lhs.y * rhs.y;
		pDst->z = lhs.z * rhs.z;
	}

	template<class Type>
	inline void Vector3TImpl<Type>::Div(Vector3TImpl* pDst, const Vector3TImpl & lhs, const Vector3TImpl & rhs)
	{
		pDst->x = lhs.x / rhs.x;
		pDst->y = lhs.y / rhs.y;
		pDst->z = lhs.z / rhs.z;
	}

	template<class Type>
	inline void Vector3TImpl<Type>::Length(Type* pDst, const Vector3TImpl & vec)
	{
		*pDst = static_cast<Type>(sqrtf(static_cast<float>(vec.x*vec.x + vec.y*vec.y + vec.z*vec.z)));
	}

	template<class Type>
	inline void Vector3TImpl<Type>::LengthSquared(Type* pDst, const Vector3TImpl & vec)
	{
		*pDst = vec.x*vec.x + vec.y*vec.y + vec.z*vec.z;
	}

	template<class Type>
	inline void Vector3TImpl<Type>::Normalize(Vector3TImpl* pDst, const Vector3TImpl& vec)
	{
		Type len;
		Length(&len, vec);
		if (len > 0)
		{
			pDst->x = pDst->x / len;
			pDst->y = pDst->y / len;
			pDst->z = pDst->z / len;
		}
		else
		{
			pDst->Set(0, 0, 0);
		}
	}




	//-------------------------------------------------------------------------------------------------------------
	// Vector2TImpl
	template<class Type>
	inline Type& Vector2TImpl<Type>::operator[](const size_t & index)
	{
		return arr[index];
	}

	template<class Type>
	inline Vector2TImpl<Type>::operator Type*()
	{
		return arr;
	}

	template<class Type>
	inline Vector2TImpl<Type>::operator const Type*() const
	{
		return arr;
	}

	template<class Type>
	inline Type Vector2TImpl<Type>::GetX() const noexcept
	{
		return x;
	}

	template<class Type>
	inline Type Vector2TImpl<Type>::GetY() const noexcept
	{
		return y;
	}

	template<class Type>
	inline void Vector2TImpl<Type>::SetX(const Type & x) noexcept
	{
		this->x = x;
	}

	template<class Type>
	inline void Vector2TImpl<Type>::SetY(const Type & y) noexcept
	{
		this->y = y;
	}

	template<class Type>
	inline void Vector2TImpl<Type>::Set(const Type & x, const Type & y) noexcept
	{
		this->x = x;
		this->y = y;
	}

	template<class Type>
	inline void Vector2TImpl<Type>::Add(Vector2TImpl* pDst, const Vector2TImpl & lhs, const Vector2TImpl & rhs)
	{
		pDst->x = lhs.x + rhs.x;
		pDst->y = lhs.y + rhs.y;
	}

	template<class Type>
	inline void Vector2TImpl<Type>::Sub(Vector2TImpl* pDst, const Vector2TImpl & lhs, const Vector2TImpl & rhs)
	{
		pDst->x = lhs.x - rhs.x;
		pDst->y = lhs.y - rhs.y;
	}

	template<class Type>
	inline void Vector2TImpl<Type>::Mul(Vector2TImpl* pDst, const Vector2TImpl & lhs, const Vector2TImpl & rhs)
	{
		pDst->x = lhs.x * rhs.x;
		pDst->y = lhs.y * rhs.y;
	}

	template<class Type>
	inline void Vector2TImpl<Type>::Div(Vector2TImpl* pDst, const Vector2TImpl & lhs, const Vector2TImpl & rhs)
	{
		pDst->x = lhs.x / rhs.x;
		pDst->y = lhs.y / rhs.y;
	}

	template<class Type>
	inline void Vector2TImpl<Type>::Length(Type* pDst, const Vector2TImpl & vec)
	{
		*pDst = static_cast<Type>(sqrtf(static_cast<float>(vec.x*vec.x + vec.y*vec.y)));
	}

	template<class Type>
	inline void Vector2TImpl<Type>::LengthSquared(Type* pDst, const Vector2TImpl & vec)
	{
		*pDst = vec.x*vec.x + vec.y*vec.y;
	}

	template<class Type>
	inline void Vector2TImpl<Type>::Normalize(Vector2TImpl* pDst, const Vector2TImpl& vec)
	{
		Type len;
		Length(&len, vec);
		if (len > 0)
		{
			pDst->x = pDst->x / len;
			pDst->y = pDst->y / len;
		}
		else
		{
			pDst->Set(0, 0);
		}
	}

}
#endif // #ifdef NGFX_VEC_TARGET_GENERAL

#endif // #ifndef NGFX_INCLUDE_VECTORIMPL_GENERAL_INL