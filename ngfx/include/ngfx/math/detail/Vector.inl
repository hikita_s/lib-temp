﻿#ifndef NGFX_INCLUDE_VECTOR_INL
#define NGFX_INCLUDE_VECTOR_INL
#include <ngfx/math/Vector.h>

namespace ngfx
{
	//-------------------------------------------------------------------------------------------------------------
	// Vector4T
	template<class Type>
	inline Vector4T<Type>::Vector4T()
	{
		this->Set(0, 0, 0, 0);
	}

	template<class Type>
	inline Vector4T<Type>::Vector4T(const Type & value)
	{
		this->Set(value, value, value, value);
	}

	template<class Type>
	inline Vector4T<Type>::Vector4T(const Type & x, const Type & y, const Type & z, const Type & w)
	{
		this->Set(x, y, z, w);
	}

	template<class Type>
	inline Vector4T<Type>::Vector4T(const Type* values)
	{
		this->Set(values[0], values[1], values[2], values[3]);
	}

	template<class Type>
	inline Vector4T<Type>::Vector4T(const Vector4T<Type>& src)
	{
		this->Set(src.GetX(), src.GetY(), src.GetZ(), src.GetW());
	}

	template<class Type>
	inline Vector4T<Type> Vector4T<Type>::operator+(const Vector4T<Type>& rhs) const
	{
		Vector4T<Type> ret;
		Vector4T<Type>::Add(&ret, *this, rhs);
		return ret;
	}

	template<class Type>
	inline Vector4T<Type> Vector4T<Type>::operator+(const Type & rhs) const
	{
		Vector4T<Type> ret;
		Vector4T<Type>::Add(&ret, *this, Vector4T<Type>(rhs));
		return ret;
	}

	template<class Type>
	inline Vector4T<Type>& Vector4T<Type>::operator+=(const Vector4T<Type>& rhs)
	{
		Vector4T<Type>::Add(this, *this, rhs);
		return *this;
	}

	template<class Type>
	inline Vector4T<Type>& Vector4T<Type>::operator+=(const Type & rhs)
	{
		Vector4T<Type>::Add(this, *this, Vector4T<Type>(rhs));
		return *this;
	}

	template<class Type>
	inline Vector4T<Type> Vector4T<Type>::operator-(const Vector4T<Type>& rhs) const
	{
		Vector4T<Type> ret;
		Vector4T<Type>::Sub(&ret, *this, rhs);
		return ret;
	}

	template<class Type>
	inline Vector4T<Type> Vector4T<Type>::operator-(const Type & rhs) const
	{
		Vector4T<Type> ret;
		Vector4T<Type>::Sub(&ret, *this, Vector4T<Type>(rhs));
		return ret;
	}

	template<class Type>
	inline Vector4T<Type>& Vector4T<Type>::operator-=(const Vector4T<Type>& rhs)
	{
		Vector4T<Type>::Sub(this, *this, rhs);
		return *this;
	}

	template<class Type>
	inline Vector4T<Type>& Vector4T<Type>::operator-=(const Type & rhs)
	{
		Vector4T<Type>::Sub(this, *this, Vector4T<Type>(rhs));
		return *this;
	}

	template<class Type>
	inline Vector4T<Type> Vector4T<Type>::operator*(const Vector4T<Type>& rhs) const
	{
		Vector4T<Type> ret;
		Vector4T<Type>::Mul(&ret, *this, rhs);
		return ret;
	}

	template<class Type>
	inline Vector4T<Type> Vector4T<Type>::operator*(const Type & rhs) const
	{
		Vector4T<Type> ret;
		Vector4T<Type>::Mul(&ret, *this, Vector4T<Type>(rhs));
		return ret;
	}

	template<class Type>
	inline Vector4T<Type>& Vector4T<Type>::operator*=(const Vector4T<Type>& rhs)
	{
		Vector4T<Type>::Mul(this, *this, rhs);
		return *this;
	}

	template<class Type>
	inline Vector4T<Type>& Vector4T<Type>::operator*=(const Type & rhs)
	{
		Vector4T<Type>::Mul(this, *this, Vector4T<Type>(rhs));
		return *this;
	}

	template<class Type>
	inline Vector4T<Type> Vector4T<Type>::operator/(const Vector4T<Type>& rhs) const
	{
		Vector4T<Type> ret;
		Vector4T<Type>::Div(&ret, *this, rhs);
		return ret;
	}

	template<class Type>
	inline Vector4T<Type> Vector4T<Type>::operator/(const Type & rhs) const
	{
		Vector4T<Type> ret;
		Vector4T<Type>::Div(&ret, *this, Vector4T<Type>(rhs));
		return ret;
	}

	template<class Type>
	inline Vector4T<Type>& Vector4T<Type>::operator/=(const Vector4T<Type>& rhs)
	{
		Vector4T<Type>::Div(this, *this, rhs);
		return *this;
	}

	template<class Type>
	inline Vector4T<Type>& Vector4T<Type>::operator/=(const Type & rhs)
	{
		Vector4T<Type>::Div(this, *this, Vector4T<Type>(rhs));
		return *this;
	}

	template<class Type>
	inline Type Vector4T<Type>::Dot(const Vector4T<Type>& rhs)
	{
		return GetX() * rhs.GetX() + GetY() * rhs.GetY() + GetZ() * rhs.GetZ() + GetW() * rhs.GetW();
	}


	//-------------------------------------------------------------------------------------------------------------
	// Vector3T
	template<class Type>
	inline Vector3T<Type>::Vector3T()
	{
		this->x = static_cast<Type>(0);
		this->y = static_cast<Type>(0);
		this->z = static_cast<Type>(0);
	}

	template<class Type>
	inline Vector3T<Type>::Vector3T(const Type & value)
	{
		this->Set(value, value, value);
	}

	template<class Type>
	inline Vector3T<Type>::Vector3T(const Type & x, const Type & y, const Type & z)
	{
		this->Set(x, y, z);
	}

	template<class Type>
	inline Vector3T<Type>::Vector3T(const Type* values)
	{
		this->Set(values[0], values[1], values[2]);
	}

	template<class Type>
	inline Vector3T<Type>::Vector3T(const Vector3T<Type>& src)
	{
		this->Set(src.GetX(), src.GetY(), src.GetZ());
	}

	template<class Type>
	inline Vector3T<Type> Vector3T<Type>::operator+(const Vector3T<Type>& rhs) const
	{
		Vector3T<Type> ret;
		Vector3T<Type>::Add(&ret, *this, rhs);
		return ret;
	}

	template<class Type>
	inline Vector3T<Type> Vector3T<Type>::operator+(const Type & rhs) const
	{
		Vector3T<Type> ret;
		Vector3T<Type>::Add(&ret, *this, Vector3T<Type>(rhs));
		return ret;
	}

	template<class Type>
	inline Vector3T<Type>& Vector3T<Type>::operator+=(const Vector3T<Type>& rhs)
	{
		Vector3T<Type>::Add(this, *this, rhs);
		return *this;
	}

	template<class Type>
	inline Vector3T<Type>& Vector3T<Type>::operator+=(const Type & rhs)
	{
		Vector3T<Type>::Add(this, *this, Vector3T<Type>(rhs));
		return *this;
	}

	template<class Type>
	inline Vector3T<Type> Vector3T<Type>::operator-(const Vector3T<Type>& rhs) const
	{
		Vector3T<Type> ret;
		Vector3T<Type>::Sub(&ret, *this, rhs);
		return ret;
	}

	template<class Type>
	inline Vector3T<Type> Vector3T<Type>::operator-(const Type & rhs) const
	{
		Vector3T<Type> ret;
		Vector3T<Type>::Sub(&ret, *this, Vector3T<Type>(rhs));
		return ret;
	}

	template<class Type>
	inline Vector3T<Type>& Vector3T<Type>::operator-=(const Vector3T<Type>& rhs)
	{
		Vector3T<Type>::Sub(this, *this, rhs);
		return *this;
	}

	template<class Type>
	inline Vector3T<Type>& Vector3T<Type>::operator-=(const Type & rhs)
	{
		Vector3T<Type>::Sub(this, *this, Vector3T<Type>(rhs));
		return *this;
	}

	template<class Type>
	inline Vector3T<Type> Vector3T<Type>::operator*(const Vector3T<Type>& rhs) const
	{
		Vector3T<Type> ret;
		Vector3T<Type>::Mul(&ret, *this, rhs);
		return ret;
	}

	template<class Type>
	inline Vector3T<Type> Vector3T<Type>::operator*(const Type & rhs) const
	{
		Vector3T<Type> ret;
		Vector3T<Type>::Mul(&ret, *this, Vector3T<Type>(rhs));
		return ret;
	}

	template<class Type>
	inline Vector3T<Type>& Vector3T<Type>::operator*=(const Vector3T<Type>& rhs)
	{
		Vector3T<Type>::Mul(this, *this, rhs);
		return *this;
	}

	template<class Type>
	inline Vector3T<Type>& Vector3T<Type>::operator*=(const Type & rhs)
	{
		Vector3T<Type>::Mul(this, *this, Vector3T<Type>(rhs));
		return *this;
	}

	template<class Type>
	inline Vector3T<Type> Vector3T<Type>::operator/(const Vector3T<Type>& rhs) const
	{
		Vector3T<Type> ret;
		Vector3T<Type>::Div(&ret, *this, rhs);
		return ret;
	}

	template<class Type>
	inline Vector3T<Type> Vector3T<Type>::operator/(const Type & rhs) const
	{
		Vector3T<Type> ret;
		Vector3T<Type>::Div(&ret, *this, Vector3T<Type>(rhs));
		return ret;
	}

	template<class Type>
	inline Vector3T<Type>& Vector3T<Type>::operator/=(const Vector3T<Type>& rhs)
	{
		Vector3T<Type>::Div(this, *this, rhs);
		return *this;
	}

	template<class Type>
	inline Vector3T<Type>& Vector3T<Type>::operator/=(const Type & rhs)
	{
		Vector3T<Type>::Div(this, *this, Vector3T<Type>(rhs));
		return *this;
	}

	template<class Type>
	inline Type Vector3T<Type>::Dot(const Vector3T<Type>& rhs)
	{
		return GetX() * rhs.GetX() + GetY() * rhs.GetY() + GetZ() * rhs.GetZ();
	}

	//-------------------------------------------------------------------------------------------------------------
	// Vector2T

	template<class Type>
	inline Vector2T<Type>::Vector2T()
	{
		this->x = static_cast<Type>(0);
		this->y = static_cast<Type>(0);
	}

	template<class Type>
	inline Vector2T<Type>::Vector2T(const Type & value)
	{
		this->Set(value, value);
	}

	template<class Type>
	inline Vector2T<Type>::Vector2T(const Type & x, const Type & y)
	{
		this->Set(x, y);
	}

	template<class Type>
	inline Vector2T<Type>::Vector2T(const Type* values)
	{
		this->Set(values[0], values[1]);
	}

	template<class Type>
	inline Vector2T<Type>::Vector2T(const Vector2T<Type>& src)
	{
		this->Set(src.GetX(), src.GetY());
	}

	template<class Type>
	inline Vector2T<Type> Vector2T<Type>::operator+(const Vector2T<Type>& rhs) const
	{
		Vector2T<Type> ret;
		Vector2T<Type>::Add(&ret, *this, rhs);
		return ret;
	}

	template<class Type>
	inline Vector2T<Type> Vector2T<Type>::operator+(const Type & rhs) const
	{
		Vector2T<Type> ret;
		Vector2T<Type>::Add(&ret, *this, Vector2T<Type>(rhs));
		return ret;
	}

	template<class Type>
	inline Vector2T<Type>& Vector2T<Type>::operator+=(const Vector2T<Type>& rhs)
	{
		Vector2T<Type>::Add(this, *this, rhs);
		return *this;
	}

	template<class Type>
	inline Vector2T<Type>& Vector2T<Type>::operator+=(const Type & rhs)
	{
		Vector2T<Type>::Add(this, *this, Vector2T<Type>(rhs));
		return *this;
	}

	template<class Type>
	inline Vector2T<Type> Vector2T<Type>::operator-(const Vector2T<Type>& rhs) const
	{
		Vector2T<Type> ret;
		Vector2T<Type>::Sub(&ret, *this, rhs);
		return ret;
	}

	template<class Type>
	inline Vector2T<Type> Vector2T<Type>::operator-(const Type & rhs) const
	{
		Vector2T<Type> ret;
		Vector2T<Type>::Sub(&ret, *this, Vector2T<Type>(rhs));
		return ret;
	}

	template<class Type>
	inline Vector2T<Type>& Vector2T<Type>::operator-=(const Vector2T<Type>& rhs)
	{
		Vector2T<Type>::Sub(this, *this, rhs);
		return *this;
	}

	template<class Type>
	inline Vector2T<Type>& Vector2T<Type>::operator-=(const Type & rhs)
	{
		Vector2T<Type>::Sub(this, *this, Vector2T<Type>(rhs));
		return *this;
	}

	template<class Type>
	inline Vector2T<Type> Vector2T<Type>::operator*(const Vector2T<Type>& rhs) const
	{
		Vector2T<Type> ret;
		Vector2T<Type>::Mul(&ret, *this, rhs);
		return ret;
	}

	template<class Type>
	inline Vector2T<Type> Vector2T<Type>::operator*(const Type & rhs) const
	{
		Vector2T<Type> ret;
		Vector2T<Type>::Mul(&ret, *this, Vector2T<Type>(rhs));
		return ret;
	}

	template<class Type>
	inline Vector2T<Type>& Vector2T<Type>::operator*=(const Vector2T<Type>& rhs)
	{
		Vector2T<Type>::Mul(this, *this, rhs);
		return *this;
	}

	template<class Type>
	inline Vector2T<Type>& Vector2T<Type>::operator*=(const Type & rhs)
	{
		Vector2T<Type>::Mul(this, *this, Vector2T<Type>(rhs));
		return *this;
	}

	template<class Type>
	inline Vector2T<Type> Vector2T<Type>::operator/(const Vector2T<Type>& rhs) const
	{
		Vector2T<Type> ret;
		Vector2T<Type>::Div(&ret, *this, rhs);
		return ret;
	}

	template<class Type>
	inline Vector2T<Type> Vector2T<Type>::operator/(const Type & rhs) const
	{
		Vector2T<Type> ret;
		Vector2T<Type>::Div(&ret, *this, Vector2T<Type>(rhs));
		return ret;
	}

	template<class Type>
	inline Vector2T<Type>& Vector2T<Type>::operator/=(const Vector2T<Type>& rhs)
	{
		Vector2T<Type>::Div(this, *this, rhs);
		return *this;
	}

	template<class Type>
	inline Vector2T<Type>& Vector2T<Type>::operator/=(const Type & rhs)
	{
		Vector2T<Type>::Div(this, *this, Vector2T<Type>(rhs));
		return *this;
	}

	template<class Type>
	inline Type Vector2T<Type>::Dot(const Vector2T<Type>& rhs)
	{
		return GetX() * rhs.GetX() + GetY() * rhs.GetY();
	}
}
#endif // #ifndef NGFX_INCLUDE_VECTOR_INL