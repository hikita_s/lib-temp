﻿#ifndef NGFX_INCLUDE_VECTOR_H
#define NGFX_INCLUDE_VECTOR_H

#define NGFX_VEC_TARGET_GENERAL

namespace ngfx
{
	/**
	* @class Vector4TImpl
	* @brief 4次元ベクトル実装
	*/
	template<class Type>
	class Vector4TImpl
	{
	private:
#ifdef NGFX_VEC_TARGET_GENERAL
		union
		{
			struct
			{
				Type x;
				Type y;
				Type z;
				Type w;
			};
			Type arr[4];
		};
#endif // #ifdef NGFX_VEC_TARGET_GENERAL

	public:
		inline Type& operator[](const size_t& index);
		inline operator Type*();
		inline operator const Type*() const;
		inline Type GetX() const noexcept;
		inline Type GetY() const noexcept;
		inline Type GetZ() const noexcept;
		inline Type GetW() const noexcept;
		inline void SetX(const Type& x) noexcept;
		inline void SetY(const Type& y) noexcept;
		inline void SetZ(const Type& z) noexcept;
		inline void SetW(const Type& w) noexcept;
		inline void Set(const Type& x, const Type& y, const Type& z, const Type& w) noexcept;

		static inline void Add(Vector4TImpl* pDst, const Vector4TImpl& lhs, const Vector4TImpl& rhs);
		static inline void Sub(Vector4TImpl* pDst, const Vector4TImpl& lhs, const Vector4TImpl& rhs);
		static inline void Mul(Vector4TImpl* pDst, const Vector4TImpl& lhs, const Vector4TImpl& rhs);
		static inline void Div(Vector4TImpl* pDst, const Vector4TImpl& lhs, const Vector4TImpl& rhs);
		static inline void Length(Type* pDst, const Vector4TImpl& vec);
		static inline void LengthSquared(Type* pDst, const Vector4TImpl& vec);
		static inline void Normalize(Vector4TImpl* pDst, const Vector4TImpl& vec);
	};


	/**
	* @class Vector3TImpl
	* @brief 3次元ベクトル実装
	*/
	template<class Type>
	class Vector3TImpl
	{
	private:
#ifdef NGFX_VEC_TARGET_GENERAL
		union
		{
			struct
			{
				Type x;
				Type y;
				Type z;
			};
			Type arr[3];
		};
#endif // #ifdef NGFX_VEC_TARGET_GENERAL

	public:
		inline Type& operator[](const size_t& index);
		inline operator Type*();
		inline operator const Type*() const;
		inline Type GetX() const noexcept;
		inline Type GetY() const noexcept;
		inline Type GetZ() const noexcept;
		inline void SetX(const Type& x) noexcept;
		inline void SetY(const Type& y) noexcept;
		inline void SetZ(const Type& z) noexcept;
		inline void Set(const Type& x, const Type& y, const Type& z) noexcept;

		static inline void Add(Vector3TImpl* pDst, const Vector3TImpl& lhs, const Vector3TImpl& rhs);
		static inline void Sub(Vector3TImpl* pDst, const Vector3TImpl& lhs, const Vector3TImpl& rhs);
		static inline void Mul(Vector3TImpl* pDst, const Vector3TImpl& lhs, const Vector3TImpl& rhs);
		static inline void Div(Vector3TImpl* pDst, const Vector3TImpl& lhs, const Vector3TImpl& rhs);
		static inline void Length(Type* pDst, const Vector3TImpl& vec);
		static inline void LengthSquared(Type* pDst, const Vector3TImpl& vec);
		static inline void Normalize(Vector3TImpl* pDst, const Vector3TImpl& vec);
	};



	/**
	* @class Vector2TImpl
	* @brief 2次元ベクトル実装
	*/
	template<class Type>
	class Vector2TImpl
	{
	private:
#ifdef NGFX_VEC_TARGET_GENERAL
		union
		{
			struct
			{
				Type x;
				Type y;
			};
			Type arr[2];
		};
#endif // #ifdef NGFX_VEC_TARGET_GENERAL

	public:
		inline Type& operator[](const size_t& index);
		inline operator Type*();
		inline operator const Type*() const;
		inline Type GetX() const noexcept;
		inline Type GetY() const noexcept;
		inline void SetX(const Type& x) noexcept;
		inline void SetY(const Type& y) noexcept;
		inline void Set(const Type& x, const Type& y) noexcept;

		static inline void Add(Vector2TImpl* pDst, const Vector2TImpl& lhs, const Vector2TImpl& rhs);
		static inline void Sub(Vector2TImpl* pDst, const Vector2TImpl& lhs, const Vector2TImpl& rhs);
		static inline void Mul(Vector2TImpl* pDst, const Vector2TImpl& lhs, const Vector2TImpl& rhs);
		static inline void Div(Vector2TImpl* pDst, const Vector2TImpl& lhs, const Vector2TImpl& rhs);
		static inline void Length(Type* pDst, const Vector2TImpl& vec);
		static inline void LengthSquared(Type* pDst, const Vector2TImpl& vec);
		static inline void Normalize(Vector2TImpl* pDst, const Vector2TImpl& vec);
	};






	/**
	* @class Vector4T
	* @brief 4次元ベクトル
	*/
	template<class Type>
	class Vector4T : public Vector4TImpl<Type>
	{
	public:
		inline Vector4T();
		inline Vector4T(const Type& value);
		inline Vector4T(const Type& x, const Type& y, const Type& z, const Type& w);
		inline Vector4T(const Type* values);
		inline Vector4T(const Vector4T<Type>& src);

		inline Vector4T<Type> operator+(const Vector4T<Type>& rhs) const;
		inline Vector4T<Type> operator+(const Type& rhs) const;
		inline Vector4T<Type>& operator+=(const Vector4T<Type>& rhs);
		inline Vector4T<Type>& operator+=(const Type& rhs);
		inline Vector4T<Type> operator-(const Vector4T<Type>& rhs) const;
		inline Vector4T<Type> operator-(const Type& rhs) const;
		inline Vector4T<Type>& operator-=(const Vector4T<Type>& rhs);
		inline Vector4T<Type>& operator-=(const Type& rhs);
		inline Vector4T<Type> operator*(const Vector4T<Type>& rhs) const;
		inline Vector4T<Type> operator*(const Type& rhs) const;
		inline Vector4T<Type>& operator*=(const Vector4T<Type>& rhs);
		inline Vector4T<Type>& operator*=(const Type& rhs);
		inline Vector4T<Type> operator/(const Vector4T<Type>& rhs) const;
		inline Vector4T<Type> operator/(const Type& rhs) const;
		inline Vector4T<Type>& operator/=(const Vector4T<Type>& rhs);
		inline Vector4T<Type>& operator/=(const Type& rhs);

		inline Type Dot(const Vector4T<Type>& rhs);
	};


	/**
	* @class Vector3T
	* @brief 3次元ベクトル
	*/
	template<class Type>
	class Vector3T : public Vector3TImpl<Type>
	{
	public:
		inline Vector3T();
		inline Vector3T(const Type& value);
		inline Vector3T(const Type& x, const Type& y, const Type& z);
		inline Vector3T(const Type* values);
		inline Vector3T(const Vector3T<Type>& src);

		inline Vector3T<Type> operator+(const Vector3T<Type>& rhs) const;
		inline Vector3T<Type> operator+(const Type& rhs) const;
		inline Vector3T<Type>& operator+=(const Vector3T<Type>& rhs);
		inline Vector3T<Type>& operator+=(const Type& rhs);
		inline Vector3T<Type> operator-(const Vector3T<Type>& rhs) const;
		inline Vector3T<Type> operator-(const Type& rhs) const;
		inline Vector3T<Type>& operator-=(const Vector3T<Type>& rhs);
		inline Vector3T<Type>& operator-=(const Type& rhs);
		inline Vector3T<Type> operator*(const Vector3T<Type>& rhs) const;
		inline Vector3T<Type> operator*(const Type& rhs) const;
		inline Vector3T<Type>& operator*=(const Vector3T<Type>& rhs);
		inline Vector3T<Type>& operator*=(const Type& rhs);
		inline Vector3T<Type> operator/(const Vector3T<Type>& rhs) const;
		inline Vector3T<Type> operator/(const Type& rhs) const;
		inline Vector3T<Type>& operator/=(const Vector3T<Type>& rhs);
		inline Vector3T<Type>& operator/=(const Type& rhs);

		inline Type Dot(const Vector3T<Type>& rhs);
	};


	/**
	* @class Vector2T
	* @brief 3次元ベクトル
	*/
	template<class Type>
	class Vector2T : public Vector2TImpl<Type>
	{
	public:
		inline Vector2T();
		inline Vector2T(const Type& value);
		inline Vector2T(const Type& x, const Type& y);
		inline Vector2T(const Type* values);
		inline Vector2T(const Vector2T<Type>& src);

		inline Vector2T<Type> operator+(const Vector2T<Type>& rhs) const;
		inline Vector2T<Type> operator+(const Type& rhs) const;
		inline Vector2T<Type>& operator+=(const Vector2T<Type>& rhs);
		inline Vector2T<Type>& operator+=(const Type& rhs);
		inline Vector2T<Type> operator-(const Vector2T<Type>& rhs) const;
		inline Vector2T<Type> operator-(const Type& rhs) const;
		inline Vector2T<Type>& operator-=(const Vector2T<Type>& rhs);
		inline Vector2T<Type>& operator-=(const Type& rhs);
		inline Vector2T<Type> operator*(const Vector2T<Type>& rhs) const;
		inline Vector2T<Type> operator*(const Type& rhs) const;
		inline Vector2T<Type>& operator*=(const Vector2T<Type>& rhs);
		inline Vector2T<Type>& operator*=(const Type& rhs);
		inline Vector2T<Type> operator/(const Vector2T<Type>& rhs) const;
		inline Vector2T<Type> operator/(const Type& rhs) const;
		inline Vector2T<Type>& operator/=(const Vector2T<Type>& rhs);
		inline Vector2T<Type>& operator/=(const Type& rhs);

		inline Type Dot(const Vector2T<Type>& rhs);
	};


	/**
	* @brief Vector4TImpl<float>
	*/
	using Vector4fImpl = Vector4TImpl<float>;
	/**
	* @brief Vector3TImpl<float>
	*/
	using Vector3fImpl = Vector3TImpl<float>;
	/**
	* @brief Vector2TImpl<float>
	*/
	using Vector2fImpl = Vector2TImpl<float>;

	/**
	* @brief Vector4T<float>
	*/
	using Vector4f = Vector4T<float>;
	/**
	* @brief Vector3T<float>
	*/
	using Vector3f = Vector3T<float>;
	/**
	* @brief Vector2T<float>
	*/
	using Vector2f = Vector2T<float>;

	/**
	* @brief Vector4iImpl<int>
	*/
	using Vector4iImpl = Vector4TImpl<int>;
	/**
	* @brief Vector3iImpl<int>
	*/
	using Vector3iImpl = Vector3TImpl<int>;
	/**
	* @brief Vector2iImpl<int>
	*/
	using Vector2iImpl = Vector2TImpl<int>;

	/**
	* @brief Vector4T<int>
	*/
	using Vector4i = Vector4T<int>;
	/**
	* @brief Vector3T<int>
	*/
	using Vector3i = Vector3T<int>;
	/**
	* @brief Vector2T<int>
	*/
	using Vector2i = Vector2T<int>;

}

#ifdef NGFX_VEC_TARGET_GENERAL
#include <ngfx/math/detail/VectorImpl_General.inl>
#endif // #ifdef NGFX_VEC_TARGET_GENERAL
#include <ngfx/math/detail/Vector.inl>

#endif // #ifndef NGFX_INCLUDE_VECTOR_H