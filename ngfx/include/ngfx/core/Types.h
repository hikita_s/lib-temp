﻿#ifndef NGFX_INCLUDE_TYPES_H
#define NGFX_INCLUDE_TYPES_H

//// C library
#include <stdint.h>
#include <stdio.h>
#include <malloc.h>
#include <time.h>

//// C++ library
#include <utility>
#include <new>
#include <cmath>
#include <type_traits>
#include <iterator>
#include <vector>
#include <map>
#include <unordered_map>
#include <memory>
#include <string>
#include <mutex>

//// os
#include <tchar.h>
#include <dxgi.h>
#define NOMINMAX
#include <windows.h>
#ifdef ERROR
#undef ERROR
#endif

namespace ngfx
{
	/**
	* @enum LOG_LEVEL
	* @brief ログ出力のレベル定義
	*/
	enum LOG_LEVEL
	{
		LOG_LEVEL_FATAL,
		LOG_LEVEL_ERROR,
		LOG_LEVEL_WARN,
		LOG_LEVEL_INFO,
		LOG_LEVEL_DEBUG,
		LOG_LEVEL_TRACE,
		LOG_LEVEL_DISABLED,

		LOG_LEVEL_FORCE_INT32 = 0x7FFFFFFF
	};
}

#endif // #ifndef NGFX_INCLUDE_TYPES_H