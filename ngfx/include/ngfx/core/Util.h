﻿#ifndef NGFX_INCLUDE_MACRO_H
#define NGFX_INCLUDE_MACRO_H

namespace ngfx
{
	class IMemoryAllocator;

	#define NGFX_CALL __stdcall

	constexpr float SQRT2 = 1.41421356237f;
	constexpr float INVSQRT2 = 1.0f / SQRT2;
	constexpr float SQRT3 = 1.73205080757f;
	constexpr float PI = 3.14159265258979f;
	constexpr float PIDIV2 = PI / 2;
	constexpr float PIDIV4 = PI / 4;
	constexpr float NAPIER_E = 2.718281828f;
	constexpr float LOGe2 = 0.69314718056f; ///< @brief  log(2)
	constexpr float LOG2e = 1.44269504089f; ///< @brief  log2(e)
	constexpr size_t NGFX_OUTPUT_STRING_BUFFER_MAX_SIZE = 1024; ///< @brief (内部用) 出力するバッファの最大サイズ

	template<class T>
	inline constexpr T NGFX_CALL Abs(const T& v) noexcept
	{
		return ((v>0) ? (v) : (-v));
	};

	template<class T>
	inline constexpr T NGFX_CALL Max(const T& a, const T& b) noexcept
	{
		return (((a) < (b)) ? (b) : (a));
	};

	template<class T>
	inline constexpr T NGFX_CALL Min(const T& a, const T& b) noexcept
	{
		return (((a) > (b)) ? (b) : (a));
	};

	template<class T>
	inline constexpr T NGFX_CALL Clamp(const T& v, const T& minValue, const T& maxValue) noexcept
	{
		return (((v)<(minValue)) ? (minValue) : (((maxValue)<(v)) ? (maxValue) : (v)));
	};

	template<class T>
	inline constexpr T NGFX_CALL Saturate(const T& v) noexcept
	{
		return CLAMP(v, static_cast<T>(0), static_cast<T>(1));
	};

	template<class T>
	inline constexpr T NGFX_CALL Lerp(const T& x, const T& y, const T& s) noexcept
	{
		return CLAMP(x + (y - x)*s, x, y);
	};

	template<class T>
	inline constexpr T NGFX_CALL Smoothstep(const T& minValue, const T& maxValue, const T& x) noexcept
	{
		// https://ja.wikipedia.org/wiki/Smoothstep
		constexpr T v = SATURATE((x - minValue) / (maxValue - minValue));
		return v * v * (static_cast<T>(3) - static_cast<T>(2) * v );
	};

	template<class T>
	inline constexpr T NGFX_CALL Roundup(const T& value, const T& alignment)
	{
		return (value + (alignment - static_cast<T>(1))) & ~(alignment - static_cast<T>(1));
	}

	template<class T>
	inline constexpr float NGFX_CALL Deg2Rad(const T& degree) noexcept
	{
		return degree * PI / 180.0f;
	};

	template<class T>
	inline constexpr float NGFX_CALL Rad2Deg(const T& degree) noexcept
	{
		return degree * 180.0f / PI;
	};

	void NGFX_CALL OutputString(const char* string, const char* file, const int& line) noexcept;

	inline bool NGFX_CALL IsPowerOfTwo(const uint32_t& value) noexcept
	{
		return (value&(value - 1)) == false;
	}


	/**
	* @brief デフォルトメモリアロケータのセット
	*/
	void NGFX_CALL SetDefaultMemoryAllocator(IMemoryAllocator* pMemoryAllocator) noexcept;

	/**
	* @brief デフォルトメモリアロケータの取得
	*/
	IMemoryAllocator* NGFX_CALL GetDefaultMemoryAllocator() noexcept;

	/**
	* @brief ログ出力のレベルを設定
	*/
	void NGFX_CALL SetLogLevel(const LOG_LEVEL& level) noexcept;

	/**
	* @brief 設定されたレベルに応じてログを書き出し
	*/
	void NGFX_CALL OutputLog(const LOG_LEVEL& level, const char* string, const char* file, const int& line);

	/**
	* @brief クラスのコピーの禁止マクロ
	*/
	#define NGFX_DISALLOW_COPY_AND_ASSIGN(TypeName)		\
		private:																					\
			TypeName(const TypeName&) = delete;							\
			void operator=(const TypeName&) = delete

	/**
	* @brief (内部用) 文字列出力
	*/
	#define NGFX_OUTPUT_STRING_IMPL(x) ::ngfx::OutputString(x, __FILE__, __LINE__)

	/**
	* @brief 文字列出力
	*/
	#define NGFX_OUTPUT_STRING(x, ...)											\
		do{																								\
			char buf[NGFX_OUTPUT_STRING_BUFFER_MAX_SIZE];		\
			sprintf_s(buf, (x), __VA_ARGS__);											\
			NGFX_OUTPUT_STRING_IMPL(buf);										\
		} while (false)

	/**
	* @brief ブレークポイント
	*/
	void NGFX_CALL BreakPoint();

#if defined(_DEBUG) || defined(DEBUG)	
	/**
	* @brief アサート
	*/
	#define NGFX_ASSERT(x)																\
		do{																								\
			if(!(x))																						\
			{																								\
				NGFX_OUTPUT_STRING_IMPL(":NGFX_ASSERT("#x")");	\
				BreakPoint();																		\
			}																								\
		}while(false)
#else
	/**
	* @brief アサート
	*/
	#define NGFX_ASSERT(x)
#endif

	/**
	* @brief 静的アサート
	*/
	#define NGFX_STATIC_ASSERT(x, message) static_assert(x, message)

	#ifndef NGFX_SAFE_RELEASE
	#define NGFX_SAFE_RELEASE(p)	{ if (p) { (p)->Release(); (p)=nullptr; } }
	#endif

	#ifndef NGFX_USE_VER
	#define NGFX_USE_VER(v)	 ((void)v)
	#endif
}

#endif // #ifndef NGFX_INCLUDE_MACRO_H
