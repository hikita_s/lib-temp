﻿#ifndef NGFX_INCLUDE_MEMORY_H
#define NGFX_INCLUDE_MEMORY_H
#include <ngfx/core/Types.h>
#include <ngfx/core/Result.h>
#include <ngfx/core/Util.h>
#include <ngfx/core/IMemoryAllocator.h>
#include <ngfx/core/StlAllocator.h>

namespace ngfx
{
	/**
	* @brief メモリのアロケート
	* @param pMemoryAllocator
	* @param size アロケートするサイズ
	* @param file ファイル名(マクロ用)
	* @param line ライン番号(マクロ用)
	* @return 確保されたメモリのポインタ
	*/
	void* NGFX_CALL AllocateImpl(IMemoryAllocator* pMemoryAllocator, const size_t& size, const char* file, const int& line);

	/**
	* @brief メモリのアロケート
	* @param pMemoryAllocator
	* @param size アロケートするサイズ
	* @param file ファイル名(マクロ用)
	* @param line ライン番号(マクロ用)
	* @return 確保されたメモリのポインタ
	*/
	#define NGFX_ALLOCATE(pMemoryAllocator, size) ::ngfx::AllocateImpl( (pMemoryAllocator), (size), __FILE__, __LINE__)

	/**
	* @brief メモリの破棄
	* @param pMemoryAllocator
	* @param ptr 開放するポインタ
	*/
	void NGFX_CALL FreeImpl(IMemoryAllocator* pMemoryAllocator, void* ptr);

	/**
	* @brief メモリの破棄
	* @param pMemoryAllocator
	* @param ptr 開放するポインタ
	*/
	#define NGFX_FREE(pMemoryAllocator, ptr)					\
		do {																				\
			if(ptr!=nullptr)															\
			{																				\
				::ngfx::FreeImpl((pMemoryAllocator), (ptr));	\
				ptr = nullptr;														\
			}																				\
		}while(false)

	/**
	* @brief new
	* @param pMemoryAllocator
	* @param file ファイル名(マクロ用)
	* @param line ライン番号(マクロ用)
	* @param args コンストラクタ引数
	* @return 確保されたメモリのポインタ
	*/
	template<class Type, typename... Args>
	inline Type* NGFX_CALL NewImpl(IMemoryAllocator* pMemoryAllocator, const char* file, const int& line, Args&&... args)
	{
		return new( AllocateImpl(pMemoryAllocator, sizeof(Type), file, line) ) Type(std::forward<_Args>(args)...);
	}

	/**
	* @brief new
	* @param pMemoryAllocator
	* @param Type 型
	* @param ... コンストラクタ引数
	* @return 確保されたメモリのポインタ
	*/
	#define NGFX_NEW(pMemoryAllocator, Type, ...) ::ngfx::NewImpl<Type>((pMemoryAllocator), __FILE__, __LINE__, __VA_ARGS__)

	/**
	* @brief delete
	* @param pMemoryAllocator
	* @param ptr 開放するポインタ
	*/
	template<class Type>
	inline void NGFX_CALL DeleteImpl(IMemoryAllocator* pMemoryAllocator, Type* ptr)
	{
		NGFX_ASSERT(ptr != nullptr);
		(ptr)->~Type();
		FreeImpl(pMemoryAllocator, ptr);
	}

	/**
	* @brief delete
	* @param pMemoryAllocator
	* @param Type 型
	* @param ptr 開放するポインタ
	*/
	#define NGFX_DELETE(pMemoryAllocator, Type, ptr)						\
		do {																									\
			if(ptr!=nullptr)																				\
			{																									\
				::ngfx::DeleteImpl<Type>((pMemoryAllocator), (ptr));		\
				ptr = nullptr;																			\
			}																									\
		}while(false)

	/**
	* @brief 配列new
	* @param pMemoryAllocator
	* @param size 配列サイズ
	* @param file ファイル名(マクロ用)
	* @param line ライン番号(マクロ用)
	* @param args コンストラクタ引数
	* @return 確保されたメモリのポインタ
	*/
	template<class Type, typename... Args>
	inline Type* NGFX_CALL NewArrayImpl(IMemoryAllocator* pMemoryAllocator, const size_t& size, const char* file, const int& line, Args&&... args)
	{
		void* ptr = AllocateImpl(pMemoryAllocator, sizeof(Type) * size + sizeof(size_t), file, line);
		(*reinterpret_cast<size_t*>(ptr)) = size;
		Type* pRet = reinterpret_cast<Type*>(reinterpret_cast<intptr_t>(ptr) + sizeof(size_t));
		for (size_t i = 0; i < size; ++i)
		{
			new(&pRet[i]) Type(std::forward<Args>(args)...);
		}
		return pRet;
	}

	/**
	* @brief new
	* @param pMemoryAllocator
	* @param Type 型
	* @param size 配列サイズ
	* @param ... コンストラクタ引数
	* @return 確保されたメモリのポインタ
	*/
	#define NGFX_NEW_ARRAY(pMemoryAllocator, Type, size, ...) ::ngfx::NewArrayImpl<Type>((pMemoryAllocator), (size), __FILE__, __LINE__, __VA_ARGS__)

	/**
	* @brief 配列delete
	* @param pMemoryAllocator
	* @param ptr 破棄する配列
	*/
	template<class Type>
	inline void NGFX_CALL DeleteArrayImpl(IMemoryAllocator* pMemoryAllocator, Type* ptr)
	{
		NGFX_ASSERT(ptr != nullptr);
		void* pHead = reinterpret_cast<void*>(reinterpret_cast<intptr_t>(ptr) - sizeof(size_t));
		const size_t itemNum = *reinterpret_cast<size_t*>(pHead);
		for (size_t i = 0; i < itemNum; ++i)
		{
			ptr[i].~Type();
		}
		FreeImpl(pMemoryAllocator, pHead);
	}

	/**
	* @brief delete
	* @param pMemoryAllocator
	* @param Type 型
	* @param ptr 開放するポインタ
	*/
	#define NGFX_DELETE_ARRAY(pMemoryAllocator, Type, ptr)				\
		do {																										\
			if(ptr!=nullptr)																					\
			{																										\
				::ngfx::DeleteArrayImpl<Type>((pMemoryAllocator), (ptr));	\
				ptr = nullptr;																				\
			}																										\
		}while(false)

	/**
	* @class SharedPtr
	* @brief std::shared_ptr
	*/
	template <class T>
	using SharedPtr = std::shared_ptr<T>;

	/**
	* @class WeakPtr
	* @brief std::weak_ptr
	*/
	template <class T>
	using WeakPtr = std::weak_ptr<T>;

	/**
	* @class UniquePtrDeleter
	* @brief DefaultMemoryAllocatorを利用したデリータ
	*/
	template<class T>
	struct UniquePtrDeleter
	{
	public:
		UniquePtrDeleter(IMemoryAllocator* pMemoryAllocator)
			: m_pMemoryAllocator(pMemoryAllocator)
		{}
		~UniquePtrDeleter() {}

		inline void operator()(T* ptr)
		{
			NGFX_DELETE_ARRAY(m_pMemoryAllocator, T, ptr);
		}
	private:
		IMemoryAllocator* m_pMemoryAllocator;
	};

	/**
	* @class UniquePtr
	* @brief std::unique_ptr
	*/
	template <class T, class D = UniquePtrDeleter<T>>
	using UniquePtr = std::unique_ptr<T, D>;

	/**
	* @brief std::make_shared
	* @param pMemoryAllocator
	* @param file ファイル名(マクロ用)
	* @param line ライン番号(マクロ用)
	* @param args コンストラクタ引数
	* @return 確保されたメモリのポインタ
	*/
	template<class Type, class... Args> inline
	SharedPtr<Type> NGFX_CALL MakeSharedImpl(IMemoryAllocator* pMemoryAllocator, char* file, const int& line, Args&&... args)
	{
		return std::allocate_shared<Type>(StlAllocator<Type>(file, line), std::forward<Args>(args)...);
	};

	/**
	* @brief std::make_shared
	* @param pMemoryAllocator
	* @param Type 型
	* @param ... コンストラクタ引数
	* @return 確保されたメモリのポインタ
	*/
	#define NGFX_MAKE_SHARED(pMemoryAllocator, Type, ...) ::ngfx::MakeSharedImpl<typename Type>((pMemoryAllocator), __FILE__, __LINE__, __VA_ARGS__)

	/**
	* @brief std::make_unique
	* @param pMemoryAllocator
	* @param file ファイル名(マクロ用)
	* @param line ライン番号(マクロ用)
	* @param args コンストラクタ引数
	* @return 確保されたメモリのポインタ
	*/
	template<class Type, class... Args> inline
	typename std::enable_if<!std::is_array<Type>::value, UniquePtr<Type, typename UniquePtrDeleter<Type>> >::type
	NGFX_CALL MakeUniqueImpl(IMemoryAllocator* pMemoryAllocator, char* file, const int& line, Args&&... args)
	{
		return UniquePtr<Type, typename  UniquePtrDeleter<Type>>(
			NewImpl<Type>(pMemoryAllocator, file, line, std::forward<Args>(args)...),
			UniquePtrDeleter<Type>(pMemoryAllocator)
		);
	}

	/**
	* @brief std::make_unique
	* @param pMemoryAllocator
	* @param Type 型
	* @param ... コンストラクタ引数
	* @return 確保されたメモリのポインタ
	*/
	#define NGFX_MAKE_UNIQUE(pMemoryAllocator, Type, ...) ::ngfx::MakeUniqueImpl<typename Type>((pMemoryAllocator), __FILE__, __LINE__, __VA_ARGS__)

};

#endif //#define NGFX_INCLUDE_MEMORY_H