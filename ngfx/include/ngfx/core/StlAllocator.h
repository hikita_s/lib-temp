﻿#ifndef NGFX_INCLUDE_STLALLOCATOR_H
#define NGFX_INCLUDE_STLALLOCATOR_H
#include <ngfx/core/Util.h>

namespace ngfx
{
	/**
	* @class StlAllocator
	* @brief std::allocatorのIAllocatorを利用する実装
	*/
	template<class T>
	class StlAllocator : public std::allocator<T>
	{
		template<class U>
		friend class StlAllocator;
	public:
		StlAllocator(char* file, const int& line)
			: m_pMemoryAllocator(GetDefaultMemoryAllocator())
			, m_File(file)
			, m_Line(line)
		{}

		StlAllocator()
			: m_pMemoryAllocator(GetDefaultMemoryAllocator())
			, m_File(__FILE__)
			, m_Line(__LINE__)
		{}


		~StlAllocator()
		{}

		template<class U>
		StlAllocator(const StlAllocator<U>& rhs)
			: m_pMemoryAllocator(GetDefaultMemoryAllocator())
			, m_File(rhs.m_File)
			, m_Line(rhs.m_Line)
		{}

		pointer allocate(size_type n, const_pointer hint = 0)
		{
			const size_t size = n * sizeof(T);
			void* p = nullptr;
			const Result result = m_pMemoryAllocator->Allocate(&p, size, m_Alignment, m_File, m_Line);
			NGFX_ASSERT(result.IsSucceeded());
			return reinterpret_cast<pointer>(p);
		}

		void deallocate(pointer ptr, size_type n)
		{
			const Result result = m_pMemoryAllocator->Free(ptr);
			NGFX_ASSERT(result.IsSucceeded());
		}

		template<class U>
		struct rebind
		{
			typedef StlAllocator<U> other;
		};

	private:
		IMemoryAllocator * m_pMemoryAllocator;
		char* m_File;
		int m_Line;
	};
};

#endif //#define NGFX_INCLUDE_STLALLOCATOR_H