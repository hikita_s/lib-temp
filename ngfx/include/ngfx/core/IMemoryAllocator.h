﻿#ifndef NGFX_INCLUDE_IMEMORYALLOCATOR_H
#define NGFX_INCLUDE_IMEMORYALLOCATOR_H
#include <ngfx/core/Types.h>
#include <ngfx/core/Result.h>

namespace ngfx
{
	/**
	* @struct MemoryBlockInfo
	* @brief メモリブロック情報
	*/
	struct MemoryBlockInfo
	{
		size_t Size;
		const char* File;
		int Line;
		uint32_t Id;
	};

	/**
	* @class IMemoryAllocator
	* @brief メモリアロケータのインターフェース
	*/
	class IMemoryAllocator
	{
	public:
		/**
		* @brief メモリのアロケート
		* @param pRet 確保するポインタへのポインタ
		* @param size 確保するメモリのByte数
		* @param file ファイル名(マクロ用)
		* @param line ライン番号(マクロ用)
		* @return アロケートが成功したならばOK
		*/
		virtual Result Allocate(void** pRet, const size_t& size, const char* file, const int& line) = 0;

		/**
		* @brief メモリの開放
		* @param p 開放するポインタ
		* @return 開放が成功したならばOK
		*/
		virtual Result Free(void* p) = 0;
	};
};

#endif //#define NGFX_INCLUDE_IMEMORYALLOCATOR_H