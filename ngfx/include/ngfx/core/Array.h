﻿#ifndef NGFX_INCLUDE_ARRAY_H
#define NGFX_INCLUDE_ARRAY_H

#include <ngfx/core/Types.h>
#include <ngfx/core/IMemoryAllocator.h>
#include <ngfx/core/Memory.h>

namespace ngfx
{
	template <class T>
	class Array
	{
		NGFX_DISALLOW_COPY_AND_ASSIGN(Array);
	public:
		Array()
			: m_apItem(nullptr)
			, m_Size(0)
			, m_pMemoryAllocator(nullptr)
		{}

		Array(IMemoryAllocator* pAllocator)
			: m_apItem(nullptr)
			, m_Size(0)
			, m_pMemoryAllocator(pAllocator)
		{}

		template <class... Types>
		Array(IMemoryAllocator* pAllocator, const size_t& size, Types&&... args)
			: m_apItem(nullptr)
			, m_Size(0)
			, m_pMemoryAllocator(nullptr)
		{
			Initialize(pAllocator, size, std::forward<Types>(args)...);
		};

		~Array()
		{
			NGFX_DELETE_ARRAY(m_pMemoryAllocator, T, m_apItem);
			m_Size = 0;
		}

		Array(Array&& rhs)
			: m_apItem(rhs.m_apItem)
			, m_Size(rhs.m_Size)
			, m_pMemoryAllocator(rhs.m_pMemoryAllocator)
		{
			rhs.m_apItem = nullptr;
			rhs.m_Size = 0;
			rhs.m_pMemoryAllocator = nullptr;
		}

		Array& operator=(Array&& rhs)
		{
			NGFX_DELETE_ARRAY(m_pMemoryAllocator, T, m_apItem);
			m_apItem = rhs.m_apItem;
			m_Size = rhs.m_Size;
			m_pMemoryAllocator = rhs.m_pMemoryAllocator;
			rhs.m_apItem = nullptr;
			rhs.m_Size = 0;
			rhs.m_pMemoryAllocator = nullptr;
			return *this;
		}

		template <class... Types>
		void Initialize(IMemoryAllocator* pAllocator, const size_t& size, Types&&... args)
		{
			NGFX_DELETE_ARRAY(m_pMemoryAllocator, T, m_apItem);
			m_pMemoryAllocator = pAllocator;
			if (size > 0) m_apItem = NGFX_NEW_ARRAY(m_pMemoryAllocator, T, size, std::forward<Types>(args)...);
			m_Size = size;
		}

		template <class... Types>
		void Resize(const size_t& size, Types&&... args)
		{
			if (m_Size < size)
			{
				Initialize(m_pMemoryAllocator, size, std::forward<Types>(args)...);
			}
		}

		inline size_t length() const noexcept
		{
			return m_Size;
		}

		inline T& operator[](const size_t& index) const noexcept
		{
			NGFX_ASSERT(m_apItem != nullptr);
			NGFX_ASSERT(index < m_Size);
			return m_apItem[index];
		}

		inline operator T*() const noexcept
		{
			return m_apItem;
		}

		inline T& operator->() const noexcept
		{
			NGFX_ASSERT(m_apItem != nullptr);
			return *m_apItem;
		}

	private:
		IMemoryAllocator* m_pMemoryAllocator;
		T* m_apItem;
		size_t m_Size;
	};
}

#endif // #ifndef NGFX_INCLUDE_ARRAY_H