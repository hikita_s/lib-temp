﻿#ifndef NGFX_INCLUDE_CORE_H
#define NGFX_INCLUDE_CORE_H

#include <ngfx/core/Types.h>
#include <ngfx/core/Result.h>
#include <ngfx/core/Util.h>

#include <ngfx/core/IMemoryAllocator.h>
#include <ngfx/core/HeapMemoryAllocator.h>
#include <ngfx/core/StlAllocator.h>

#include <ngfx/core/Array.h>

namespace ngfx
{
	/**
	* @class String
	* @brief std::basic_string
	*/
	using String = std::basic_string<char, std::char_traits<char>, typename StlAllocator<char> >;

	/**
	* @class ArrayList
	* @brief std::vector
	*/
	template<class T, class Allocator = typename StlAllocator<T>>
	using ArrayList = std::vector<T, Allocator>;

	/**
	* @class List
	* @brief std::list
	*/
	template<class T, class Allocator = typename StlAllocator<T>>
	using List = std::vector<T, Allocator>;

	/**
	* @class HashMap
	* @brief std::unordered_map
	*/
	template <
		class Key,
		class Value,
		class Hash = typename std::hash<Key>,
		class Pred = typename std::equal_to<Key>,
		class Allocator = typename StlAllocator<std::pair<const Key, Value>>
	>
	using HashMap = std::unordered_map<Key, Value, Hash, Pred, Allocator>;

	/**
	* @class TreeMap
	* @brief std::map
	*/
	template <
		class Key,
		class Value,
		class Compare = typename std::less<Key>,
		class Allocator = typename StlAllocator<std::pair<const Key, Value>>
	>
	using TreeMap = std::map<Key, Value, Compare, Allocator>;
}

#endif // #ifndef NGFX_INCLUDE_CORE_H