﻿#ifndef NGFX_INCLUDE_HEAPMEMORYALLOCATOR_H
#define NGFX_INCLUDE_HEAPMEMORYALLOCATOR_H
#include <ngfx/core/IMemoryAllocator.h>

namespace ngfx
{
	/**
	* @class HeapMemoryAllocator
	* @brief ヒープを利用するメモリアロケーター
	*/
	class HeapMemoryAllocator : public IMemoryAllocator
	{
	public:
		HeapMemoryAllocator();
		~HeapMemoryAllocator();

		/**
		* @brief メモリのアロケート
		* @param pRet 確保するポインタへのポインタ
		* @param size 確保するメモリのByte数
		* @param file ファイル名(マクロ用)
		* @param line ライン番号(マクロ用)
		* @return アロケートが成功したならばOK
		*/
		virtual Result Allocate(void** pRet, const size_t& size, const char* file, const int& line) override;

		/**
		* @brief メモリの開放
		* @param p 開放するポインタ
		* @return 開放が成功したならばOK
		*/
		virtual Result Free(void* p) override;

	private:
		/**
		* @struct MemoryHeader開放
		* @brief メモリヘッダ
		*/
		struct MemoryHeader : public MemoryBlockInfo
		{
			MemoryHeader* pNext;
			MemoryHeader* pPrev;
		};

	public:
		/**
		* @class AllocatedMemoryIterator
		* @brief アロケートされているメモリヘッダを返すイテレータ
		*/
		class AllocatedMemoryIterator
		{
		private:
			friend HeapMemoryAllocator;
			AllocatedMemoryIterator(MemoryHeader* pHeader);
			~AllocatedMemoryIterator();
		public:
			AllocatedMemoryIterator & operator++();
			AllocatedMemoryIterator operator++(int);
			bool operator==(const AllocatedMemoryIterator& iterator);
			bool operator!=(const AllocatedMemoryIterator& iterator);
			const MemoryBlockInfo& operator->();
			const MemoryBlockInfo& operator*();
		private:
			MemoryHeader* m_pHeader;
		};

		/**
		* @brief 現在アロケートしているメモリブロックのイテレータを返す
		*/
		AllocatedMemoryIterator AllocateMemoryIteratorBegin() const noexcept;

		/**
		* @brief 現在アロケートしているメモリブロックのイテレータの終端を返す
		*/
		AllocatedMemoryIterator AllocateMemoryIteratorEnd() const noexcept;

	private:
		MemoryHeader * m_pFront; ///< @brief メモリヘッダーリストの先頭
		MemoryHeader* m_pBack; ///< @brief メモリヘッダーリストの最後尾
		uint32_t m_CurrentMemoryBlockId; ///< @brief ユニークなIDのカウンタ
	};
};

#endif //#define NGFX_INCLUDE_HEAPMEMORYALLOCATOR_H