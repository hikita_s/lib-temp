﻿#ifndef NGFX_INCLUDE_RESULT_H
#define NGFX_INCLUDE_RESULT_H
#include <ngfx/core/Types.h>

namespace ngfx
{
	class Result
	{
	protected:
		Result(const uint32_t& code) noexcept;
	public:
		Result();
		~Result();

		/**
		* @brief この結果が成功であるならばtrue
		*/
		inline operator bool() const  noexcept
		{
			return IsSucceeded();
		}

		/**
		* @brief この結果が成功であるならばtrue
		*/
		bool IsSucceeded() const noexcept;

		/**
		* @brief この結果が失敗であるならばtrue
		*/
		bool IsFailed() const noexcept;

		inline uint32_t GetCode() const noexcept
		{
			return m_Code;
		}
	public:
		static const Result OK;
		static const Result FAIL;
	private:
		uint32_t m_Code;
	};

}


#endif // #ifndef NGFX_INCLUDE_RESULT_H