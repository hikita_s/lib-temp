﻿#ifndef NGFX_INCLUDE_WINDOW_H
#define NGFX_INCLUDE_WINDOW_H
#include <ngfx/core/Core.h>

namespace ngfx
{
	/**
	* @brief Windowをリサイズするときのコールバック関数
	*/
	using ResizeCallback = void(*)(const UINT& uiWidth, const UINT& uiHeight, void* pUserData);

	/**
	* @enum WINDOW_CREATE_DESC
	*/
	struct WINDOW_CREATE_DESC
	{
		WINDOW_CREATE_DESC()
			: Name(_T("Window"))
			, Width(1280)
			, Height(720)
			, hInstance(nullptr)
			, ResizeCallback(nullptr)
			, pUserData(nullptr)
		{}

		TCHAR*			Name;
		UINT			Width;
		UINT			Height;
		HINSTANCE		hInstance;
		ResizeCallback ResizeCallback;
		void* pUserData;
	};

	/**
	* @class Window
	*/
	class Window
	{
	public:
		Window();
		virtual ~Window();
		HRESULT Create(const WINDOW_CREATE_DESC& desc);
		void Show() const;
		bool IsQuit() const noexcept;
		void BeginFrame();
		void GetWindowSize(UINT* pWidth, UINT* pHeight);

		inline HWND GetHWND() const noexcept
		{
			return m_hWnd;
		}

	private:
		LRESULT CALLBACK WindowProcImpl(HWND hwnd, UINT Msg, WPARAM wParam, LPARAM lParam);
		static LRESULT CALLBACK WindowProc(HWND hwnd, UINT Msg, WPARAM wParam, LPARAM lParam);
	private:
		HWND m_hWnd;
		MSG m_MSG;
		ResizeCallback m_ResizeCallback;
		void* m_pUserData;
	};
}

#endif // #ifndef NGFX_INCLUDE_NGFX_H